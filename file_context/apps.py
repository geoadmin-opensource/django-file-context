# -*- coding: utf-8
from django.apps import AppConfig


class FileContextConfig(AppConfig):
    name = 'file_context'
