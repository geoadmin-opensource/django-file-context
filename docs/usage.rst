=====
Usage
=====

To use Django File Context in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'file_context.apps.FileContextConfig',
        ...
    )

Add Django File Context's URL patterns:

.. code-block:: python

    from file_context import urls as file_context_urls


    urlpatterns = [
        ...
        url(r'^', include(file_context_urls)),
        ...
    ]
