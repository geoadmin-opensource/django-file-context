============
Installation
============

At the command line::

    $ easy_install django-file-context

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv django-file-context
    $ pip install django-file-context
