# coding: utf-8
from django.test import SimpleTestCase
from file_context.utils import load_module_member


def mock_one():
    return 'foo'


class LoadModuleMemberTestCase(SimpleTestCase):

    def test_raises_value_error_if_none(self):

        with self.assertRaises(ValueError):
            load_module_member(None)

    def test_load_module_member(self):

        fn = load_module_member('tests.test_utils.mock_one')
        self.assertIsNotNone(fn)

    def test_handle_exception(self):

        fn = load_module_member('foooo.bar')
        self.assertIsNone(fn)
