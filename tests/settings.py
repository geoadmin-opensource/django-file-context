# -*- coding: utf-8
from __future__ import unicode_literals, absolute_import
import os
import django

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DEBUG = True
USE_TZ = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "b%ind_r=c+h15$hle$7p&8(6+qf)jji-tef0%8r9)w@^0s+=q6"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": ":memory:",
    }
}

ROOT_URLCONF = "tests.urls"

INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sites",
    "django_filters",
    "rest_framework",
    "rest_framework_filters",
    "taggit",
    "taggit_serializer",
    "common",
    "file_context",
]

SITE_ID = 1

if django.VERSION >= (1, 10):
    MIDDLEWARE = ()
else:
    MIDDLEWARE_CLASSES = ()

REST_FRAMEWORK = {
    'DEFAULT_FILTER_BACKENDS': ('rest_framework_filters.backends.DjangoFilterBackend',),
}

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'chaos', 'locale'),
)