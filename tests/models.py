# coding: utf-8
from django.db import models
from django_fake_model import models as fake
from file_context.managers import Files


class FakeContainer(fake.FakeModel):

    name = models.CharField(max_length=10)

    files = Files()
