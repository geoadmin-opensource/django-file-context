# coding: utf-8
from django.utils.timezone import now
from django.test import TransactionTestCase
from file_context.models import File
from file_context.serializers import (
    FileSerializer,
    FileContextSerializer,
)


class FileViewSetTestCase(TransactionTestCase):

    def test_init(self):

        serializer = FileSerializer()
        self.assertIsNotNone(serializer)

    def test_serialize(self):
        rite_now = now()
        file = File.objects.create(
            name='foo',
            date_created=rite_now,
            date_updated=rite_now,
        )
        serializer = FileSerializer(file, context={'request': None})
        data = serializer.data
        self.assertIsNotNone(data)
        self.assertTrue(isinstance(data, dict))
        self.assertIn('id', data)
        self.assertIn('created_by', data)
        self.assertIn('name', data)
        self.assertIn('document', data)
        self.assertIn('context', data)
        self.assertIn('tags', data)
        self.assertIn('links', data)