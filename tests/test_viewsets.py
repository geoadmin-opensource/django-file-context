# coding: utf-8
from rest_framework.test import APITransactionTestCase
from rest_framework.reverse import reverse
from file_context.models import File


class FileViewSetTestCase(APITransactionTestCase):

    def test_retrieve(self):

        file = File.objects.create(name='foo')
        url = reverse('file-detail', kwargs={'pk': file.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)