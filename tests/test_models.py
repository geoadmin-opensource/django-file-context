# -*- coding: utf-8 -*-
from django.test import TransactionTestCase
from tests.models import FakeContainer
from file_context.models import File, FileContext


@FakeContainer.fake_me
class FileTestCase(TransactionTestCase):

    def test_init(self):

        doc = File.objects.create(
            name='foo'
        )
        container = FakeContainer.objects.create(name='foo')
        doc.attach(container)
        self.assertEqual(FileContext.objects.all().count(), 1)
        self.assertEqual(container.files.all().count(), 1)
        self.assertEqual(container.files.all()[0].pk, doc.pk)

    def test_attach(self):

        doc = File.objects.create(
            name='foo'
        )
        container = FakeContainer.objects.create(name='foo')
        container.files.attach(doc)
        self.assertEqual(FileContext.objects.all().count(), 1)
        self.assertEqual(container.files.all().count(), 1)
        self.assertEqual(container.files.all()[0].pk, doc.pk)

    def test_attach_two_files(self):

        doc1 = File.objects.create(
            name='foo'
        )
        doc2 = File.objects.create(
            name='foo'
        )
        container = FakeContainer.objects.create(name='container')
        container.files.attach(doc1)
        container.files.attach(doc2)
        self.assertEqual(FileContext.objects.all().count(), 2)
        self.assertEqual(container.files.all().count(), 2)
        pks = [doc1.pk, doc2.pk]
        self.assertIn(container.files.all()[0].pk, pks)
        self.assertIn(container.files.all()[1].pk, pks)

    def test_attach_two_files_args(self):

        doc1 = File.objects.create(
            name='foo'
        )
        doc2 = File.objects.create(
            name='foo'
        )
        container = FakeContainer.objects.create(name='container')
        container.files.attach(*[doc1, doc2])
        self.assertEqual(FileContext.objects.all().count(), 2)
        self.assertEqual(container.files.all().count(), 2)
        pks = [doc1.pk, doc2.pk]
        self.assertIn(container.files.all()[0].pk, pks)
        self.assertIn(container.files.all()[1].pk, pks)

    def test_detach(self):

        doc = File.objects.create(
            name='foo'
        )
        container = FakeContainer.objects.create(name='foo')
        container.files.attach(doc)
        self.assertEqual(FileContext.objects.all().count(), 1)
        self.assertEqual(container.files.all().count(), 1)
        self.assertEqual(container.files.all()[0].pk, doc.pk)
        container.files.detach(doc)
        self.assertEqual(FileContext.objects.all().count(), 0)
        self.assertEqual(File.objects.all().count(), 1)
