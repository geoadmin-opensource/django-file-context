# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.conf.urls import url, include

from file_context.urls import urlpatterns as file_context_urls

urlpatterns = [
    url(r'^', include(file_context_urls)),
]
